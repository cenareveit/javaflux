package com.it.cenareve.javaflux.annotations;

import com.it.cenareve.javaflux.Store;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyBind {
    String property ();
    Class<? extends Store> store();
}
