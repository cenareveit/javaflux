package com.it.cenareve.javaflux;

import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public abstract class Controller implements Initializable {
    protected URL location;
    protected ResourceBundle resources;

    @Override
    public void initialize(URL location, ResourceBundle resources){
        this.location = location;
        this.resources = resources;
    }
}
