package com.it.cenareve.javaflux;

public class Action<T extends Enum, P> {
    private T type;

    private P payload;

    public Action(T type, P payload) {
        this.type = type;
        this.payload = payload;
    }

    public P getPayload() {
        return payload;
    }

    public T getType() {
        return type;
    }
}
