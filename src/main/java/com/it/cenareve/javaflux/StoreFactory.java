package com.it.cenareve.javaflux;

import java.util.HashMap;
import java.util.Map;

public class StoreFactory {
    private static Map<Class<? extends Store >,Store> storeMap = new HashMap<>();
    public static Store get(Class<? extends Store> cls) throws IllegalAccessException, InstantiationException {
        if (storeMap.get(cls)==null) {
            Store store = cls.newInstance();
            store.init();
            storeMap.put(cls, store);
        }
        return storeMap.get(cls);
    }
    private StoreFactory(){}
}
