package com.it.cenareve.javaflux;

import java.util.function.Consumer;
import java.util.function.Predicate;

public abstract class Store {
    protected Dispatcher dispatcher;

    public Store() {
        dispatcher = Dispatcher.getInstance();
    }

    public void registerActionHandler(Predicate<? super Action> predicate, Consumer<Action> consumer) {
        dispatcher.getEventStream().filter(predicate).subscribe(consumer);
    }

    protected abstract void init();
}
