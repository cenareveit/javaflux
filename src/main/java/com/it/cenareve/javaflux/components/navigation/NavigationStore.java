package com.it.cenareve.javaflux.components.navigation;

import com.it.cenareve.javaflux.Action;
import com.it.cenareve.javaflux.Store;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.Stack;

public class NavigationStore extends Store {

    private Stack<String> routeStack = new Stack<>();
    private Stack<String> cachedRouteStack = new Stack<>();

    @Override
    protected void init() {
        registerActionHandler(action -> action.getType() == NavigationActions.Type.NAVIGATE, this::navigateTo);
        registerActionHandler(action -> action.getType() == NavigationActions.Type.PREVIOUS, this::navigateBack);
        registerActionHandler(action -> action.getType() == NavigationActions.Type.NEXT, this::navigateFront);
    }

    /**
     * Change the current route to the next value
     *
     * @param action
     */
    private void navigateFront(Action action) {
        String route;
        if ((route = cachedRouteStack.pop()) != null) {
            routeStack.push(route);
        }
    }

    /**
     * Change the current route to previous value
     *
     * @param action
     */
    private void navigateBack(Action<NavigationActions.Type, String> action) {
        String route;
        if ((route = routeStack.pop()) != null) {
            cachedRouteStack.push(route);
            updateRoute();
        }
    }

    /**
     * Change the route and save the precedent path
     *
     * @param action
     */
    private void navigateTo(Action<NavigationActions.Type, String> action) {
        routeStack.add(action.getPayload());
        cachedRouteStack.clear();
        updateRoute();
    }

    /**
     * Update the route property
     */
    private void updateRoute() {
        currentRoute.set(routeStack.peek());
    }

    // store properties
    private SimpleStringProperty currentRoute = new SimpleStringProperty();
    private SimpleBooleanProperty hasPrevious = new SimpleBooleanProperty();
    private SimpleBooleanProperty hasNext = new SimpleBooleanProperty();

    public String getCurrentRoute() {
        return currentRoute.get();
    }

    public SimpleStringProperty currentRouteProperty() {
        return currentRoute;
    }

    public void setCurrentRoute(String currentRoute) {
        this.currentRoute.set(currentRoute);
    }

    public boolean isHasPrevious() {
        return hasPrevious.get();
    }

    public SimpleBooleanProperty hasPreviousProperty() {
        return hasPrevious;
    }

    public void setHasPrevious(boolean hasPrevious) {
        this.hasPrevious.set(hasPrevious);
    }

    public boolean isHasNext() {
        return hasNext.get();
    }

    public SimpleBooleanProperty hasNextProperty() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext.set(hasNext);
    }
}
