package com.it.cenareve.javaflux.components.navigation;

import com.it.cenareve.javaflux.StoreFactory;
import javafx.beans.property.*;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class NavigationComponent extends BorderPane implements Initializable {
    // Flux store
    private NavigationStore navigationStore;
    // properties
    private SimpleStringProperty currentRoute = new SimpleStringProperty(this, "currentRoute");
    private SimpleBooleanProperty hasPrevious = new SimpleBooleanProperty(this, "hasPrevious");
    private SimpleBooleanProperty hasNext = new SimpleBooleanProperty(this, "hasNext");
    private ListProperty<NavigationPane> pages = new SimpleListProperty<>(this, "pages");
    private ObjectProperty<Image> logo = new SimpleObjectProperty<>(this, "logo");

    @FXML
    private NavigationBar navigationBar;
    @FXML
    private NavigationToolBar navigationToolBar;

    public NavigationComponent() throws InstantiationException, IllegalAccessException {
        navigationStore = (NavigationStore) StoreFactory.get(NavigationStore.class);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("navigation-component.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initSubComponent();
        initNavigationBar();
        initRouteProperty();
    }

    private void initSubComponent() {
        // BavigationBar
        navigationBar.logoProperty().bind(logo);
        // Navigation toolbar
        navigationToolBar.routeProperty().bind(currentRoute);
        navigationToolBar.hasNextProperty().bind(hasNext);
        navigationToolBar.hasPreviousProperty().bind(hasPrevious);
    }

    private void initNavigationBar() {
        pages.addListener((ListChangeListener<? super NavigationPane>) c -> {
            if (c.next()) {
                for (NavigationPane pane : c.getAddedSubList()) {
                    String route = getRoute(pane);
                    if (isRootRoute(route) && !existsRootRoute(route)) {
                        NavigationButton btn = new NavigationButton();
                        btn.setText((pane).getLabel());
                        navigationBar.getChildren().add(btn);
                    }
                }
            }
        });
    }

    private boolean isRootRoute(String route) {
        return route.startsWith("/") && route.split("/").length == 2;
    }

    private boolean existsRootRoute(String route) {
        for (Node button : navigationBar.getChildren()) {
            if (button instanceof NavigationButton) {
                if (route.equals(((NavigationButton) button).getText()))
                    return true;
            }
        }
        return false;
    }

    private void initRouteProperty() {
        currentRoute.bind(navigationStore.currentRouteProperty());
        hasNext.bind(navigationStore.hasNextProperty());
        hasPrevious.bind(navigationStore.hasPreviousProperty());
        currentRoute.addListener((observable, oldValue, newValue) -> {
            for (Node pane : pages) {
                String route = getRoute((NavigationPane) pane);
                if (route.equals(currentRoute.getValue())) {
                    setCenter(pane);
                    break;
                } else {
                }
            }
        });
    }

    // Property getters and setters
    public ListProperty<NavigationPane> pagesProperty() {
        return pages;
    }

    public ObservableList<NavigationPane> getPages() {
        return pages.get();
    }

    public void setPages(ObservableList<NavigationPane> pages) {
        this.pages.set(pages);
    }

    // Layout contraints
    static void setConstraint(Node node, Object key, Object value) {
        if (value == null) {
            node.getProperties().remove(key);
        } else {
            node.getProperties().put(key, value);
        }
        if (node.getParent() != null) {
            node.getParent().requestLayout();
        }
    }

    static Object getConstraint(Node node, Object key) {
        if (node.hasProperties()) {
            Object value = node.getProperties().get(key);
            if (value != null) {
                return value;
            }
        }
        return null;
    }

    private static final String ROUTE_CONSTRAINT = "navigation-route";

    public static String getRoute(Node child) {
        return String.valueOf(getConstraint(child, ROUTE_CONSTRAINT));
    }

    public static void setRoute(Node child, String route) {
        setConstraint(child, ROUTE_CONSTRAINT, route);
    }

    public Image getLogo() {
        return logo.get();
    }

    public ObjectProperty<Image> logoProperty() {
        return logo;
    }

    public void setLogo(Image logo) {
        this.logo.set(logo);
    }
}
