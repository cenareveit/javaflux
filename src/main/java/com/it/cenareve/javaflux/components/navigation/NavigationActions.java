package com.it.cenareve.javaflux.components.navigation;

public class NavigationActions {
    public enum Type {
        NAVIGATE, PREVIOUS,
        NEXT, ADD_ROUTE, EDIT_ROUTE, REMOVE_ROUTE
    }

    public static void navigate(String route) {
    }

    public static void previous(String route) {
    }

    public static void next(String route) {
    }

    public static void addRoute(String route) {
    }

    public static void editRoute(String route) {
    }

    public static void removeRoute(String route) {
    }
}
