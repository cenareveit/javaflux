package com.it.cenareve.javaflux.components.navigation;

import javafx.beans.property.*;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.util.Map;

public class NavigationBar extends HBox {
    private ListProperty<String> buttons;
    private IntegerProperty selectedIndex;
    private ObjectProperty<Image> logo = new SimpleObjectProperty<>(this, "image");
    @FXML
    private ImageView imageView;

    public NavigationBar() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("navigation-bar.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initialize() {
        imageView.imageProperty().bind(logo);
    }

    public Image getLogo() {
        return logo.get();
    }

    public ObjectProperty<Image> logoProperty() {
        return logo;
    }

    public void setLogo(Image logoUrl) {
        this.logo.set(logoUrl);
    }

    public ObservableList<String> getButtons() {
        return buttons.get();
    }

    public ListProperty<String> buttonsProperty() {
        return buttons;
    }

    public void setButtons(ObservableList<String> buttons) {
        this.buttons.setValue(buttons);
    }
}
