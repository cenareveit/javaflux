package com.it.cenareve.javaflux.components.navigation;

import javafx.beans.DefaultProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

import java.io.IOException;

@DefaultProperty("content")
public class NavigationPane extends Parent {
    private StringProperty label = new SimpleStringProperty(this,"label");
    private StringProperty route = new SimpleStringProperty(this,"route");
    private ObjectProperty<Region> content = new SimpleObjectProperty<>(this,"content");
    public NavigationPane() {
        super();
    }

    public String getRoute() {
        return route.get();
    }

    public StringProperty routeProperty() {
        return route;
    }

    public void setRoute(String route) {
        this.route.set(route);
    }

    public Region getContent() {
        return content.get();
    }

    public ObjectProperty<Region> contentProperty() {
        return content;
    }

    public void setContent(Region content) {
        this.content.set(content);
    }


    public String getLabel() {
        return label.get();
    }

    public StringProperty labelProperty() {
        return label;
    }

    public void setLabel(String label) {
        this.label.set(label);
    }
}
