package com.it.cenareve.javaflux.components.navigation;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.FlowPane;

import java.io.IOException;

public class NavigationToolBar extends FlowPane {
    private SimpleStringProperty route = new SimpleStringProperty(this,"route");
    private SimpleBooleanProperty hasNext = new SimpleBooleanProperty(this,"hasNext");
    private SimpleBooleanProperty hasPrevious = new SimpleBooleanProperty(this,"hasPrevious");
    public NavigationToolBar() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("NavigationToolBar.fxml"));
        loader.setRoot(this);
        loader.setController(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isHasNext() {
        return hasNext.get();
    }

    public SimpleBooleanProperty hasNextProperty() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext.set(hasNext);
    }

    public boolean isHasPrevious() {
        return hasPrevious.get();
    }

    public SimpleBooleanProperty hasPreviousProperty() {
        return hasPrevious;
    }

    public void setHasPrevious(boolean hasPrevious) {
        this.hasPrevious.set(hasPrevious);
    }

    public String getRoute() {
        return route.get();
    }

    public SimpleStringProperty routeProperty() {
        return route;
    }

    public void setRoute(String route) {
        this.route.set(route);
    }
}
