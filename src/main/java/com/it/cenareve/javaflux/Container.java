package com.it.cenareve.javaflux;

import com.it.cenareve.javaflux.annotations.PropertyBind;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Container extends Controller{
    private static final Logger LOGGER = Logger.getLogger(Container.class.getName());

    @Override
    public void initialize(URL location, ResourceBundle resources){
        super.initialize(location,resources);
        bindProperties(this);
    }

    private void bindProperties(Controller controller) {

        for (Field field : controller.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                PropertyBind annotation = field.getAnnotation(PropertyBind.class);
                if (annotation != null) {
                    Class<? extends Store> cls  =annotation.store();
                    Store store = StoreFactory.get(cls);
                    Field storeField = cls.getDeclaredField(annotation.property());
                    storeField.setAccessible(true);
                    Object o = storeField.get(store);

                    field.set(controller, o);
                }

            } catch (InstantiationException|IllegalAccessException | NoSuchFieldException e) {
                LOGGER.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
