package com.it.cenareve.javaflux;

import org.reactfx.EventSource;
import org.reactfx.EventStream;

public class Dispatcher {

    private static Dispatcher ourInstance = new Dispatcher();

    public static Dispatcher getInstance() {
        return ourInstance;
    }


    private EventSource<Action> eventStream = new EventSource<>();

    private Dispatcher() {
    }

    public void dispatch(Action action){
        eventStream.push(action);
    }

    public EventSource<Action> getEventStream() {
        return eventStream;
    }
}
