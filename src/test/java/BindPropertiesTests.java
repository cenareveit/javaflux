import com.it.cenareve.javaflux.Container;
import com.it.cenareve.javaflux.Controller;
import com.it.cenareve.javaflux.Store;
import com.it.cenareve.javaflux.StoreFactory;
import com.it.cenareve.javaflux.annotations.PropertyBind;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import org.junit.Assert;
import org.junit.Test;

/*public class BindPropertiesTests {
    public static class Store1 extends Store{

        public Store1() {
            super();
        }
        public Property p1 = new SimpleBooleanProperty(true);
    }
    public static class Controller1 extends Controller {

        public Controller1() {
            super();
        }

        @PropertyBind(store = Store1.class,property = "p1")
        public Property prop1;
    }
    public static class Container1 extends Container{
        public Container1(Controller c){
            super(c);
        }
    }


    @Test
    public void bindPublicProperty() throws InstantiationException, IllegalAccessException {

        Store1 s =(Store1) StoreFactory.get(Store1.class);

        Controller1 c = new Controller1();
        Container ct = new Container1(c);
        Assert.assertNotNull (c.prop1);
        Assert.assertTrue((Boolean) c.prop1.getValue());
        s.p1.setValue(false);
        Assert.assertFalse((Boolean) c.prop1.getValue());

    }



    public static class Store2 extends Store{

        public Store2() {
            super();
        }
        private Property p1 = new SimpleBooleanProperty(true);
    }
    public static class Controller2 extends Controller {

        public Controller2() {
            super();
        }

        @PropertyBind(store = Store2.class,property = "p1")
        private Property prop1;
    }
    public static class Container2 extends Container{
        public Container2(Controller c){
            super(c);
        }
    }
    @Test
    public void bindPrivateProperty() throws InstantiationException, IllegalAccessException {
        Store2 s =(Store2) StoreFactory.get(Store2.class);

        Controller2 c = new Controller2();
        Container ct = new Container2(c);
        Assert.assertNotNull (c.prop1);
        Assert.assertTrue((Boolean) c.prop1.getValue());
        s.p1.setValue(false);
        Assert.assertFalse((Boolean) c.prop1.getValue());
    }
}*/
